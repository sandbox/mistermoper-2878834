<?php
/**
 * @file
 * Module pages.
 */


/**
 * Settings form.
 */
function ms_dynamics_oauth2_settings_form($form, &$form_state) {
  $variable_base = 'ms_dynamics_oauth2_';


  $url = variable_get($variable_base . 'url', NULL);
  $client_id = variable_get($variable_base . 'client_id', NULL);
  $client_secret = variable_get($variable_base . 'client_secret', NULL);
  $client_url = variable_get($variable_base . 'client_url');

  $form['ms_dynamics_oauth2_conf'] = array('#type' => 'fieldset', '#title' => t('Connection settings'));
  $form['ms_dynamics_oauth2_conf'][$variable_base . 'url'] = array(
    '#title' => t('API Client url'),
    '#type'   => 'textfield',
    '#default_value' => $url,
  );

  $form['ms_dynamics_oauth2_conf'][$variable_base . 'client_url'] = array(
    '#title' => t('Client url'),
    '#type'   => 'textfield',
    '#default_value' => $client_url,
  );

  $form['ms_dynamics_oauth2_conf'][$variable_base . 'client_id'] = array(
    '#title' => t('Client id'),
    '#type'   => 'textfield',
    '#default_value' => $client_id,
  );

  $form['ms_dynamics_oauth2_conf'][$variable_base . 'client_secret'] = array(
    '#title' => t('Client secret'),
    '#type'   => 'textfield',
    '#default_value' => $client_secret,
  );

  $form['ms_dynamics_oauth2_conf']['connect'] = array(
    '#markup' => l(
      'Authorize',
      '/ms-dynamics-oauth2/authorize'
    ),
    '#type' => 'markup',
    '#access' => !empty($url) && !empty($client_id) && !empty($client_secret),
  );

  $form['ms_dynamics_oauth_notifications'] = array('#type' => 'fieldset', '#title' => t('Notifications'));
  $form['ms_dynamics_oauth_notifications']['ms_dynamics_oauth_unreachable_notify'] = array(
    '#type'  => 'checkbox',
    '#title' => t('Notify when connection is lost'),
    '#description' => t('If enabled, a email will be sent to a email list when CRM connection is lost.'),
    '#default_value' => variable_get('ms_dynamics_oauth_unreachable_notify'),
  );

  $form['ms_dynamics_oauth_notifications']['ms_dynamics_oauth_unreachable_notify_mails'] = array(
    '#type'  => 'textarea',
    '#title' => t('Mails to notify'),
    '#description' => t('This mails will be notified when CRM connection is lost. Separated by lines'),
    '#default_value' => variable_get('ms_dynamics_oauth_unreachable_notify_mails'),
    '#states' => array(
      'visible' => array(
       ':input[name="ms_dynamics_oauth_unreachable_notify"]' => array('checked' => TRUE),
      ),
    ),
  );

  return system_settings_form($form);
}

/**
 * Validate callback for settings form.
 */
function ms_dynamics_oauth2_settings_form_validate(&$form, &$form_state) {
  // If notify field is enabled, add mails list.
  if (!empty($form_state['values']['ms_dynamics_oauth_unreachable_notify']) && empty($form_state['values']['ms_dynamics_oauth_unreachable_notify_mails'])){
    form_error(
      $form['ms_dynamics_oauth_notifications']['ms_dynamics_oauth_unreachable_notify_mails'],
      t('The field %field is required.',
        array('%field' => $form['ms_dynamics_oauth_notifications']['ms_dynamics_oauth_unreachable_notify_mails']['#title'])
      )
    );
  }
}

/**
 * Authorize callback.
 */
function ms_dynamics_oauth2_authorize_callback() {
  variable_del('ms_dynamics_oauth2_token');
  $client = ms_dynamics_oauth2_client_load();
  $token = $client->getMSDynamicsToken();
  if (!empty($token)) {
    variable_set('ms_dynamics_oauth2_token', $token);
    ms_dynamics_oauth2_status_reset_cache();
  }
  drupal_set_message('Authorization completed!');
  drupal_goto('admin/config/services/ms-dynamics-oauth2');
}

/**
 * Page callback: View resources list.
 */
function ms_dynamics_oauth2_resources_view() {
  $resources = ms_dynamics_oauth2_get_resources();
  $resources_table_render = array(
    '#theme' => 'ms_dynamics_oauth2_resources',
    '#resources' => $resources,
  );
  return $resources_table_render;
}
