<?php
/**
 * @file
 * Contains MSDynamicsOauth2\Client.
 */
namespace MSDynamicsOauth2;

require_once drupal_get_path('module', 'oauth2_client') . '/oauth2_client.inc';

/**
 * Class to process oauth2 requests on ms dynamics.
 */
class Client extends \OAuth2\Client {

  /**
   * {@inheritdoc}
   */
  public function __construct($params = NULL, $id = NULL) {
    parent::__construct($params, $id);
    if (empty(array_filter($this->token))) {
      $this->token = variable_get('ms_dynamics_oauth2_token', array());
    }
  }

  /**
   * Public version of getTokenRefreshToken().
   *
   * @see MSDynamicsOauth2\Client::getTokenRefreshToken().
   * 
   * @return type
   */
  public function getRefreshToken() {
    $token = $this->getTokenRefreshToken();
    $token['expiration_time'] = REQUEST_TIME + $token['expires_in'];
    $this->token = $token;
    return $this->token;
  }

  public function getMSDynamicsToken() {
    $token = $this->getTokenServerSide();
    $token['expiration_time'] = REQUEST_TIME + $token['expires_in'];
    $this->token = $token;
    return $this->token;
  }

  /**
   * {@inheritdoc}
   */
  protected function getTokenRefreshToken() {
    if (!$this->token['refresh_token']) {
      watchdog('ms_dynamics_oauth2', 'There is no refresh token', array(), WATCHDOG_ERROR);
      return;
    }
    return $this->getToken(array(
      'scope' => 'openid',
      'grant_type' => 'refresh_token',
      'resource'      => $this->params['resource'],
      'refresh_token' => $this->token['refresh_token'],
      'client_id'     => $this->params['client_id'],
      'client_secret' => $this->params['client_secret'],
    ));
  }

  /**
   * Overrides MSDynamicsOauth2::getTokenServerSide().
   *
   * If code is set, client id and client secret are set in data.
   */
  protected function getTokenServerSide() {
    if (!isset($_GET['code'])) {
      drupal_goto($this->getAuthenticationUrl() . '&api-version=1.0');
    }
    else {
      $this->token = $this->getToken(array(
        'grant_type'    => 'authorization_code',
        'code'          => $_GET['code'],
        'resource'      => $this->params['resource'],
        'redirect_uri'  => $this->params['redirect_uri'],
        'client_id'     => $this->params['client_id'],
        'client_secret' => $this->params['client_secret'],
      ));
      return $this->token;
    }
  }

  public function getToken($data) {
    if (array_key_exists('scope', $data) and $data['scope'] === NULL) {
      unset($data['scope']);
    }

    $token_endpoint = $this->params['token_endpoint'] . '?api-version=1.0';

    $options = array(
      'method' => 'POST',
      'data' => drupal_http_build_query($data),
    );
    $result = drupal_http_request($token_endpoint, $options);

    if ($result->code != 200) {
      $vars = array(
        '@grant_type' => $data['grant_type'],
        '@result_error' => $result->error,
      );
      watchdog('ms_dynamics_oauth2', 'Failed to get an access token of grant_type @grant_type.\nError: @result_error', $vars, WATCHDOG_ERROR);
      return;
    }

    return (Array) json_decode($result->data);
  }
}